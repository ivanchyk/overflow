function showPage() {
	setTimeout(function () {
		// document.body.classList.remove('js-loading');
		setTimeout(function () {
			$("#overlay").fadeOut();
		}, 100);
	});
}

$(document).ready(function () {
	"use strict";

	//custom select
	$('select').selectric();


	$('[data-popup-trigger]').on('click', function(e){
		e.preventDefault();
		$('body').addClass('opened--popup');
		var $anchor = $(this);
		$($anchor.attr('href')).addClass('popup--open');
	})

	$('[data-popup-close]').on('click', function(e){
		e.preventDefault();
		$('body').removeClass('opened--popup');
		$('.popup').removeClass('popup--open');
	});

	/**
     * Escape popup
     */
    window.addEventListener('keyup', function (event) {
        if (event.key === 'Escape') {
            $('body').removeClass('opened--popup');
				$('.popup').removeClass('popup--open');
        }
    });


});
$(window).on('load', showPage);